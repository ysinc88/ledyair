class Product < ActiveRecord::Base

translates :description
belongs_to :product_category
belongs_to :user
has_many :product_images

has_many  :leds,  through: :led_configurations
has_and_belongs_to_many :led_configurations


#has_and_belongs_to_many :led_configurations_products
#has_many  :led_configurations,  through: :led_configurations_products

# scope :visible, lambda { where(:visible => true) }
# scope :invisible, lambda { where(:visible => false) }
 scope :sortedp, lambda { order("products.price ASC") }
# scope :newest_first, lambda { order("pages.created_at DESC")}

include FriendlyId
friendly_id :name, :use => [:slugged]

 end
