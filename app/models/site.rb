class Site < ActiveRecord::Base

has_many :navigations
has_many :pages
has_one :store
has_many :product_categories, :through => :store
has_many :sections, :through => :pages
has_many :products, :through => :product_categories
has_many :galleries

 include FriendlyId
  friendly_id :name, :use => [:slugged]
end
