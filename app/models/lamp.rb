class Lamp < ActiveRecord::Base

has_many :joiners
has_many :constituents, through: :joiners
has_many :constituents_types, through: :constituents
has_many :product_images

accepts_nested_attributes_for :constituents, :joiners, :product_images

scope :sortedp, lambda { order("lamps.sell ASC") }



include FriendlyId
friendly_id :model, :use => [:slugged]

end
