class Joiner < ActiveRecord::Base

belongs_to :constituent
belongs_to :lamp
belongs_to :constituent_type

scope :leds, -> {where(constituent_type_id: 2)}


end