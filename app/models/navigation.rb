class Navigation < ActiveRecord::Base

belongs_to :site
scope :sorted, lambda { order("navigations.order ASC") }
scope :visible, lambda {where(:display => true)}
translates :name

end
