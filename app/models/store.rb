class Store < ActiveRecord::Base

belongs_to :site
has_many :product_categories

 include FriendlyId
  friendly_id :name, :use => [:slugged]
end
