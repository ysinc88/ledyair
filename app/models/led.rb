class Led < ActiveRecord::Base

has_and_belongs_to_many :led_configurations
translates :name, :description

end
