class Image < ActiveRecord::Base

belongs_to :gallery
 scope :sorted, lambda { order("images.position ASC") }
 
end
