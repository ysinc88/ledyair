class ProductImage < ActiveRecord::Base

belongs_to :product
belongs_to :lamp
scope :sorted, lambda { order("product_images.order ASC") }


end
