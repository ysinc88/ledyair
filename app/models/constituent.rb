class Constituent < ActiveRecord::Base

belongs_to :constituent_type
has_many :joiners
has_many :lamps, through: :joiners

translates :name, :description

scope :leds, -> {where(constituent_type_id: 2)}
scope :heatsinks, -> {where(constituent_type_id: 1)}
scope :psus, -> {where(constituent_type_id: 3)}
scope :fans, -> {where(constituent_type_id: 4)}

end