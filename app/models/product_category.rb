class ProductCategory < ActiveRecord::Base

belongs_to :store
has_many :products

 include FriendlyId
  friendly_id :name, :use => [:slugged]
end
