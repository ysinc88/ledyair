class Page < ActiveRecord::Base

belongs_to :site
has_many :sections
has_many :images
has_many :hovers

 include FriendlyId
  friendly_id :name, :use => [:slugged]
end
