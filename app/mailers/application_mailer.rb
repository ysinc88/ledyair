class ApplicationMailer < ActionMailer::Base
  default to: "onelove@ledyair.com"
  default from: "onelove@ledyair.com"
  layout 'mailer'
  

end
