class ContactMailer < ApplicationMailer

  default to: 'onelove@ledyair.com'
  
  def contact_email(name, email, phone, body)
        @name = name
        @email = email
        @phone = phone
        @body = body

        mail(from: email, subject: 'Contact Request')
  end
end