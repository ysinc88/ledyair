class JoinsController < ApplicationController
  before_action :set_join, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @joins = Join.all
    respond_with(@joins)
  end

  def show
    respond_with(@join)
  end

  def new
    @join = Join.new
    respond_with(@join)
  end

  def edit
  end

  def create
    @join = Join.new(join_params)
    @join.save
    respond_with(@join)
  end

  def update
    @join.update(join_params)
    respond_with(@join)
  end

  def destroy
    @join.destroy
    respond_with(@join)
  end

  private
    def set_join
      @join = Join.find(params[:id])
    end

    def join_params
      params.require(:join).permit(:lamp_id, :constituent_id, :constituent_count)
    end
end
