class Admin::NavigationsController < Admin::BaseAdminController

def index
  @site = Site.find(1)
  @navigation = Navigation.where(:site_id => 1)
end

def show
  @navigation = Navigation.find(params[:id])
end

  def new
    @navigation = Navigation.new({:site_id => 1})
  end

  def create
    @navigation = Navigation.new(navigation_params)
     if @navigation.save
      flash[:notice] = "Navigation created successfully."
      redirect_to(:action => 'index')
    else
      render('new')
    end
  end
  
  def edit
    @navigation = Navigation.find(params[:id])
  end
  
  def update
    @navigation = Navigation.find(params[:id])
    if @navigation.update_attributes(navigation_params)
      flash[:notice] = "Navigation updated"
      redirect_to(:action => 'index')
    else
      flash[:notice] = "Navigation not updated"
      render ('edit')
    end
   end
  
  
     private

    def navigation_params
      params.require(:navigation).permit(:name, :order, :display, :controller, :site_id, :href, :updated_at, :css_class)
    end
    
end
