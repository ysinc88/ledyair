class Admin::StoresController < Admin::BaseAdminController
  def index
  @page = Page.find(4)
  @title = @page.title
  @stores = Store.all
  
  end
  
  def show
    @store = Store.find(params[:id])
  end
  
  def new
    @store = Store.new
  end
  
  def create
    @store = Store.new(store_params)
    if @store.save
      flash[:success] = "Store created successfully"
      redirect_to polymorphic_path([:admin, @store])
    else
      @errors = @store.errors.full_messages
      render 'new'
    end
  end
  
  private
  
      def store_params
        params.require(:store).permit(:name, :site_id)
      end
end
