class Admin::ProductImagesController < Admin::BaseAdminController

def index
  @product_images = ProductImage.all
end

def new
  @product_image = ProductImage.new
  @products = Product.all
end

def create
  @product_image = ProductImage.new(product_image_params)
  if @product_image.save
  flash[:success] = "Image added successfully"
  redirect_to  admin_product_images_path
  else
  flash[:notice] = "Woops"
  render 'new'
  end
end

private

  def product_image_params
  params.require(:product_image).permit(:product_id, :src, :alt, :order, :feature)
  end

end
