class Admin::PagesController < Admin::BaseAdminController


def index
  @pages = Page.all
end

def show
  @page = Page.friendly.find(params[:id])
  @sections = @page.sections
  #@sections = Section.joins(:page)
end

def new
  @page = Page.new({:site_id => 1})
end

def create
  @page = Page.new(page_params)
  if @page.save
  flash[:success] = "Yay, page created"
  redirect_to admin_sites_path
  else
  flash[:notice] = "Woops, failed creating page"
  render 'new'
  end
end

def destroy
  @page = Page.find(params[:id])
  if @page.destroy
  flash[:notice] = "Page destroyed"
  redirect_to(admin_pages)
  else
  flash[:notice] = "Woops"
  end
end

private

def page_params
params.require(:page).permit(:site_id, :title, :permalink, :slug)
end

end