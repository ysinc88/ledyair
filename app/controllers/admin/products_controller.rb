class Admin::ProductsController < Admin::BaseAdminController
  def index
    @products = Product.all
  end
  
  def show
    @products = Product.all
    @product = Product.friendly.find(params[:id])
    @ledconf = @product.led_configurations
  end
  
  def new
    
    @product = Product.new
    @product_categories = ProductCategory.all
    
  end
  
  def create
    @product = Product.new(product_params)
    if @product.save
    flash[:success] = "Product created"
    redirect_to admin_products_path
    else
    flash[:notice] = "Woops"
    render 'new'
    end
    
  end
  
  def edit
    @product = Product.friendly.find(params[:id])
    @product_categories = ProductCategory.all
    @pimages = @product.product_images
    @led_configurations = LedConfiguration.all
    @leds = Led.all
  end
  
  def update
    @product = Product.friendly.find(params[:id])
    if @product.update_attributes(product_params)
    flash[:success] = "Product updated"
    redirect_to admin_products_path
    else
    flash[:warning] = "Woops"
    render "edit"
    end
  end
  
  
  private
  
  def product_params
    params.require(:product).permit(
      :product_category_id, 
      :name, 
      :description, 
      :price, 
      :dimensions, 
      :led_count, 
      :lighting_area, 
      :watt, 
      :stock,
      {:led_ids => []}
      )
  end
end
