class Admin::SectionsController < Admin::BaseAdminController
  def index
    @sections = Section.all
  end

  def edit
    @section = Section.find(params[:id])
    @pages = Page.all
  end
  
  def update
    @section = Section.find(params[:id])
    if @section.update_attributes(section_params)
      flash[:notice] = "Section updated"
      redirect_to(:action => 'index')
    else
      flash[:notice] = "Section not updated"
      render ('edit')
    end
   end

  
  def new
    @section = Section.new
  end
  
  def create
    @section = Section.new(section_params)
    if @section.save
    flash[:success] = "Section created successfully"
    redirect_to admin_page_path(@page)
    else
    flash[:notice] = "Problem creating section"
    render 'new'
    end
  end
  

  def delete
  end

  def destroy
  end
  
  private
  
  def section_params
    params.require(:section).permit(:header, :content, :image, :page_id)
  end
end
