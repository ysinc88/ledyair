class Admin::UsersController < Admin::BaseAdminController
  def index
  end
  
  def create
    user = User.new(user_params)
    if user.save
      session[:user_id] = user.id
      redirect_to admin_pages_path
      flash[:success] = "User created"
    else
      redirect_to admin_signup_path
    end
  end
  
  def show
    @user = User.find(params[:id])
    
  end

  def delete
  end

  def edit
  end
  
  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
  
end
