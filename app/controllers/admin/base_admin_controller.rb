class Admin::BaseAdminController < ApplicationController
  
  before_filter :authorize
  protect_from_forgery with: :exception
  
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def authorize
    redirect_to admin_login_path unless current_user
  end
  
  layout "layouts/admin"
end