class Admin::LedsController < Admin::BaseAdminController
  def index
    @leds = Led.all
    @newled = Led.new()
    
    def wave(wl)
      if Led.wl > 1000
      @wave = "K"
      else
      "nm"
      end
    end
    
  end

  def new
    @leds = Led.all
    
  end
  
  def create
    @led =Led.new(led_params)
    if @led.save
    flash[:success] = "Product created"
    redirect_to admin_leds_path
    else
    flash[:notice] = "Woops"
    render 'new'
    end
    
  end
  def update
  end
  
  private
   def led_params
      params.require(:led).permit(:name,:wl,:fv,:amp,:color,:price,:currency)
   end
end
