class Admin::SitesController < Admin::BaseAdminController

   def index
    @sites = Site.all
    
  end

  def show
    @site = Site.friendly.find(params[:id])
    #@pages = Page.joins(:site)
    @pages = @site.pages
    @navigation = @site.navigations
    @product_categories = @site.product_categories
  end

  def new
    @site = Site.new
  end

  def create
    @site = Site.new(site_params)
     if @site.save
      flash[:notice] = "Site created successfully."
      redirect_to(:action => 'index')
    else
      render('new')
    end
  end

  def destroy
  end
  
   private

    def site_params
      params.require(:site).permit(:name, :url, :description, :created_at, :updated_at)
    end
    
end
