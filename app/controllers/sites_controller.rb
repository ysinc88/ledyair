class SitesController < ApplicationController
  def index
    @sites = Site.find(1).all
  end

  def show
    @site = Site.friendly.find(params[:id])
  end

    
end
