require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
	before_filter :get_navigation,  :set_locale, :page_title
  after_filter :cache_headers
  
  
  def cache_headers
    if request.xhr?
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
    end
  end
  
  
  def get_navigation
    @colors = ["red","green","blue"]
    @navigation = Navigation.joins(:site).sorted.visible
    
  end
  
  def get_site
  
  
    @site = Site.find(1)
    @pages = Page.joins(:site)
    @action = params[:action].to_s
    @he_font = 'Alef'
    @en_font = 'Noto Sans'
    @hovers = Hover.all
    #@sections = Section.all
   end
  
  def page_title
    title = t "company"
    action = t "action.#{@action}"
    
    @title = title.to_s + " | " + action.to_s
  end


  private
    def set_locale
      if params[:locale]
        I18n.default_locale = params[:locale]
        I18n.locale = params[:locale]  
      end
    end
    
    def default_url_options(options = {})
      {locale: I18n.locale}
    end
    
    def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end
    helper_method :current_user
    
    
    
end
