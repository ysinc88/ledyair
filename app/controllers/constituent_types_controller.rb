class ConstituentTypesController < ApplicationController
  before_action :set_constituent_type, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @constituent_types = ConstituentType.all
    respond_with(@constituent_types)
  end

  def show
    respond_with(@constituent_type)
  end

  def new
    @constituent_type = ConstituentType.new
    respond_with(@constituent_type)
  end

  def edit
  end

  def create
    @constituent_type = ConstituentType.new(constituent_type_params)
    @constituent_type.save
    respond_with(@constituent_type)
  end

  def update
    @constituent_type.update(constituent_type_params)
    respond_with(@constituent_type)
  end

  def destroy
    @constituent_type.destroy
    respond_with(@constituent_type)
  end

  private
    def set_constituent_type
      @constituent_type = ConstituentType.find(params[:id])
    end

    def constituent_type_params
      params.require(:constituent_type).permit(:name, :description)
    end
end
