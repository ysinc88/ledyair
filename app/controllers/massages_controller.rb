class MassagesController < ApplicationController
  def index
    
  end
  
  
  def create
    @message = Message.create!(message_params)
  end
  
  
  private
  
  def message_params
  params.require(:message).permit(:user_id,:content,:created_at,:updated_at)
  end
end
