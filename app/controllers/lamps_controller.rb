class LampsController < ApplicationController
  before_action :set_lamp, only: [:show, :edit, :update, :destroy]

  respond_to :html
 layout 'admin'
 
  def index
    @lamps = Lamp.all
  end

  def show
    respond_with(@lamp)
  end

  def new
    @lamp = Lamp.new
    @lamp.constituents.build
    @lamp.product_images.build
    @lamp.joiners.build
    @ctype = ConstituentType.all
    respond_with(@lamp)
  end

  def edit
    @lamps = Lamp.all
  end

  def create
    @lamp = Lamp.new(lamp_params)
    @lamp.save
    respond_with(@lamp)
  end

  def update
    @lamp.update(lamp_params)
    respond_with(@lamp)
  end

  def destroy
    @lamp.destroy
    respond_with(@lamp)
  end

  private
    def set_lamp
      @lamp = Lamp.friendly.find(params[:id])
    end

    def lamp_params
      params.require(:lamp).permit(
      :model, :description, :sell, :cost, :stock, :watt, :width, :length,
      :joiners_attributes => [:constituent_id, :lamp_id, :sum]
      )
    end
end
