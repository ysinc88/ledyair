class ContactController < ApplicationController
  
    def send_mail
      
      name = params[:name]
      email = params[:email]
      phone = params[:phone]
      body = params[:comments]
      ContactMailer.contact_email(name, email, phone, body).deliver_now
      flash[:notice] = "#{t ('messsent') }"
      redirect_to contact_path
    end
    
    def new
      @message = Message.new
    end
    
    def create
      @message = Message.new(message_params)
      if @message.save
      else
      end
    end
    
    private
    
    def message_attributes
      require(:massage).permit(:user_id,:content,:created_at,:updated_at)
    end
end
