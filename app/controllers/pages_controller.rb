class PagesController < ApplicationController

    before_filter :get_sections
    #respond_to :html, :js
    skip_before_action :verify_authenticity_token

  def landing
    @title = t "landingTitle"
  end
  def about
    @action = 'about'
    respond_to do |format|
      format.html #{ redirect_to contact_url }
      format.js #{render :partial => 'gallery'}
    end
    
  end

  def original_url
      @orig_url = base_url + original_fullpath
    end


    def get_sections
    @action = params[:action].to_s
    
    if Page.find_by slug: @action
      page = Page.find_by slug: @action
      @sections = page.sections
    else
    
    end
    
  end

  
  def contact
    @messages = Message.all
    @message = Message.new
    
    respond_to do |format|
      format.html #{ redirect_to contact_url }
      format.js #{render :partial => 'about'}
    end
  end
  
  def diy
    
    respond_to do |format|
      format.html #{ render :file => "diy.js.erb" }
      format.js #{render :partial => 'gallery'}
    end
    
  end
  
  def gallery

    @images = Image.all.sorted
    #@thumbs = Dir["./app/assets/images/gallery/thumbs/**/*.jpg"]
    respond_to do |format|
      format.html #{ redirect_to gallery_url }
      format.js #{render :partial => 'gallery'}
    end
    
    
  end
  
  
  def store

    @products = Product.all.sortedp
    @lamps = Lamp.all
    @leds = Led.all
    @ledc = LedConfiguration.all
    @users = User.all
    @featureimg = ProductImage.where(:feature => 1).first

    
    config.gem 'money'
    require 'money/bank/google_currency'
    Money.default_bank = Money::Bank::GoogleCurrency.new
    
    respond_to do |format|
      format.html #{ redirect_to store_url }
      format.js #{render :partial => 'gallery'}
    end
    
  end
  
  
  
  
  def blog
    @sections = Page.find(7).sections.all
  end



end
