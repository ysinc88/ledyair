class ConstituentsController < ApplicationController
  before_action :set_constituent, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @constituents = Constituent.all
    
    respond_with(@constituent)
  end

  def show
    respond_with(@constituent)
  end

  def new
    @constituent = Constituent.new
    @type = ConstituentType.all
    respond_with(@constituent)
  end

  def edit
    @type = ConstituentType.all
  end

  def create
    @constituent = Constituent.new(constituent_params)
    @constituent.save
    redirect_to constituents_url
    respond_with(@constituent)
  end

  def update
    @constituent.update(constituent_params)
    respond_with(@constituent)
  end

  def destroy
    @constituent.destroy
    respond_with(@constituent)
  end

  private
    def set_constituent
      @constituent = Constituent.find(params[:id])
    end

    def constituent_params
      params.require(:constituent).permit(:name, :description, :cost_ils, :cost_usd, :width, :length, :volt, :amp, :watt, :weight, :constituent_type_id, :cfm)
    end
end
