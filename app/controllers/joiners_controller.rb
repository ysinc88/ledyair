class JoinersController < ApplicationController
 before_action :set_joiner, only: [:show, :edit, :update, :destroy]

  respond_to :html
  layout 'admin'
  def index
    @joiners = Joiner.all
    respond_with(@joiners)
  end

  def show
    respond_with(@joiner)
  end

  def new
    @joiner = Joiner.new
    @lamps = Lamp.all
    @constituents = Constituent.all
    respond_with(@joiner)
  end

  def edit
  end

  def create
    @joiner = Joiner.new(joiner_params)
    @joiner.save
    respond_with(@joiner)
  end

  def update
    @joiner.update(joiner_params)
    respond_with(@joiner)
  end

  def destroy
    @joiner.destroy
    respond_with(@joiner)
  end

  private
    def set_joiner
      @joiner = Joiner.find(params[:id])
    end

    def joiner_params
      params.require(:joiner).permit(:lamp_id, :constituent_id, :sum)
    end
end
