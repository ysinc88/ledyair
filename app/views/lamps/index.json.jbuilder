json.array!(@lamps) do |lamp|
  json.extract! lamp, :id, :model, :description, :sell, :cost, :stock, :watt, :width, :length, :led_sum1, :led_sum2, :led_sum3, :led_sum4
  json.url lamp_url(lamp, format: :json)
end
