json.array!(@maps) do |map|
  json.extract! map, :id, :person, :lat, :lng, :address
  json.url map_url(map, format: :json)
end
