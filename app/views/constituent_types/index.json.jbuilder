json.array!(@constituent_types) do |constituent_type|
  json.extract! constituent_type, :id, :name, :description
  json.url constituent_type_url(constituent_type, format: :json)
end
