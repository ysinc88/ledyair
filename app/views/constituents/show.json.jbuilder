json.extract! @constituent, :id, :name, :description, :cost_ils, :cost_usd, :width, :length, :volt, :amp, :watt, :weight, :type_id, :cfm, :created_at, :updated_at
