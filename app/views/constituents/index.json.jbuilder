json.array!(@constituents) do |constituent|
  json.extract! constituent, :id, :name, :description, :cost_ils, :cost_usd, :width, :length, :volt, :amp, :watt, :weight, :type_id, :cfm
  json.url constituent_url(constituent, format: :json)
end
