json.array!(@joins) do |join|
  json.extract! join, :id, :lamp_id, :constituent_id, :constituent_count
  json.url join_url(join, format: :json)
end
