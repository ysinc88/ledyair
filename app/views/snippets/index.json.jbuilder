json.array!(@snippets) do |snippet|
  json.extract! snippet, :id, :language, :snip
  json.url snippet_url(snippet, format: :json)
end
