json.array!(@joiners) do |joiner|
  json.extract! joiner, :id, :lamp_id, :constituent_id, :sum
  json.url joiner_url(joiner, format: :json)
end
