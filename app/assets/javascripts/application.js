// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require turbolinks
//= require jquery
//= require jquery_ujs
// require jquery.ui.all
//= require bootstrap-sprockets
//= require bootstrap.min
//= require jquery.magnific-popup.min
// require photoswipe
//= require bootstrap/alert
//= require bootstrap/dropdown
//= require jquery.swipebox
//= require bootstrap/modal
//= require jssor.slider
//= require bootstrap/transition
//= require custom
//= require gallery

//= require jquery.sticky
//= require jquery.slicknav
//= require owl.carousel
//= require jquery.sumtr
// require jquery.pikachoose
// require jquery.jcarousel.min
// require jquery.touchswipe.min
//= require jquery.tablesorter.min
// require ajaxify_rails
//= require jquery.bootstrap-touchspin
//= require ekko-lightbox.min
