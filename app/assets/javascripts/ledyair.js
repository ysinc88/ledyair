angular
  .module('ledyair', [
    'ui.router'
  ])
  .config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {
    //$urlRouterProvider.otherwise('/');
    $stateProvider
      
      .state('diy', {
        url: '/diy',
        templateUrl: 'dyi.html',
        controller: 'homeCtrl'
      })
      .state('about', {
        url: '/about',
        templateUrl: 'about.html',
        controller: 'aboutCtrl'
      })
      
  }])