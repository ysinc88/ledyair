Rails.application.routes.draw do
  
  resources :maps

  get "/hackita" => 'hackita#index'

  get "/test" => 'test#index'
get "/mp3s" => 'test#mp3s'

  resources :joiners

  resources :constituent_types

  resources :joins

  resources :lamps

  resources :constituents

  resources :snippets

    get 'auth/facebook/callback' => 'sessions#create'
    match 'auth/failure', to: redirect("/"), via: 'get'
    match 'signout', to: 'sessions#destroy', as: 'signout', via: 'get'
    match 'pims', to: 'public#index', as: 'pims', via: 'get'
    get "/p" => redirect("/piwik/index.php")
    get "/pims" => redirect("/pimsleur.html")

  
    namespace :admin do
    
        resources :pages, :navigations, :sites, :sections, :products, :product_categories, :product_images, :media, :costs, :leds, :led_configurations
        root to: "sites#index"
      
        get '/login' => 'sessions#new'
        post '/login' => 'sessions#create'
        get '/logout' => 'sessions#destroy'
        get '/signup' => 'users#new'
        post '/users' => 'users#create'  
        
      mount Ckeditor::Engine => '/ckeditor'
    end
      
    
  scope ":locale", locale: /#{I18n.available_locales.join("|")}/ do
    
    resource :page
    resource :message
    get 'contact' => 'pages#contact'
    get 'store' => 'pages#store'
    get 'contact' => 'pages#contact'
    get 'diy' => 'pages#diy'
    get 'gallery' => 'pages#gallery'
    get 'blog' => 'pages#blog'
    get 'about' => 'pages#about'
    match '/contact',     to: 'contacts#new',        via: 'get'
    match '/send_mail', to: 'contact#send_mail', via: 'post'
    match '*path', to: redirect { |params, request| "/#{params[:locale]}" }, via: 'get'
    root to: 'pages#landing', as: "locale_root"
  end
  
  root to: redirect("/#{I18n.default_locale}")

  resources "contact", only: [:new, :create]

end
