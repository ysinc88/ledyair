# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'
Rails.application.config.assets.precompile += %w( custom.js )
Rails.application.config.assets.precompile += %w( lang-set.js )
Rails.application.config.assets.precompile += %w( admin.css )
Rails.application.config.assets.precompile += %w( facebook.js.coffee.erb)
Rails.application.config.assets.precompile += %w( store.js )
Rails.application.config.assets.precompile += %w( jssor.js )
Rails.application.config.assets.precompile += %w( jssor.slider.js )
Rails.application.config.assets.precompile += %w( gallery.js )
#Rails.application.config.assets.precompile += %w( ga.js )
#Rails.application.config.assets.precompile += %w( lang-links.js )
#Rails.application.config.assets.precompile += %w( snipcart/he.js )
#Rails.application.config.assets.precompile += %w( snipcart/fe.js )
#Rails.application.config.assets.precompile += %w( snipcart/en.js )
# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

