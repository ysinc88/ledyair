module YourApp
  class Application < Rails::Application
    config.before_initialize do
      @navigation = Navigation.all
    end
  end
end