require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Ledyair
  class Application < Rails::Application
    # Use the responders controller from the responders gem
    config.app_generators.scaffold_controller :responders_controller
    config.action_controller.default_url_options = { :trailing_slash => true }
    #config.middleware.use Rack::Pjax
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'
    #config.assets.compile = false
    #config.assets.compress = true
    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :he
    
    config.middleware.use Rack::Deflater
    
    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
    require "i18n/backend/fallbacks" 
    I18n::Backend::Simple.send(:include, I18n::Backend::Fallbacks)
    #config.i18n.fallbacks = [:fe ,:he, :en] 
    I18n.fallbacks.map(:fe => :he)
    
    config.action_mailer.default_url_options = { :host => 'localhost:3000' }
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      address:              'smtp.gmail.com',
      port:                 587,
      domain:               'ledyair.com',
      user_name:           ENV["guser"],
      password:             ENV["gpass"],
      authentication:       'plain',
      enable_starttls_auto: true  ,
      openssl_verify_mode: 'none'
      }
  end
  
  
end
