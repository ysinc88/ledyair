# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150909163627) do

  create_table "constituent_translations", force: :cascade do |t|
    t.integer  "constituent_id", limit: 4,     null: false
    t.string   "locale",         limit: 255,   null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "name",           limit: 255
    t.text     "description",    limit: 65535
  end

  add_index "constituent_translations", ["constituent_id"], name: "index_constituent_translations_on_constituent_id", using: :btree
  add_index "constituent_translations", ["locale"], name: "index_constituent_translations_on_locale", using: :btree

  create_table "constituent_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "constituents", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.text     "description",         limit: 65535
    t.decimal  "cost_ils",                          precision: 10, scale: 2
    t.decimal  "cost_usd",                          precision: 10, scale: 2
    t.integer  "width",               limit: 4
    t.integer  "length",              limit: 4
    t.decimal  "volt",                              precision: 10, scale: 1
    t.decimal  "amp",                               precision: 10, scale: 2
    t.decimal  "watt",                              precision: 10
    t.integer  "weight",              limit: 4
    t.integer  "constituent_type_id", limit: 4
    t.integer  "cfm",                 limit: 4
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
  end

  add_index "constituents", ["constituent_type_id"], name: "index_constituents_on_constituent_type_id", using: :btree

  create_table "contents", force: :cascade do |t|
    t.integer  "page_id",    limit: 4
    t.text     "body",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "contents", ["page_id"], name: "index_contents_on_page_id", using: :btree

  create_table "costs", force: :cascade do |t|
    t.integer  "category_id", limit: 4
    t.string   "item",        limit: 255
    t.string   "slug",        limit: 255
    t.decimal  "price",                   precision: 10, scale: 2
    t.string   "currency",    limit: 255
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  create_table "galleries", force: :cascade do |t|
    t.string   "site_id",    limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "galleries", ["site_id"], name: "index_galleries_on_site_id", using: :btree

  create_table "hovers", force: :cascade do |t|
    t.string   "page_id",     limit: 255
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "handler",     limit: 255
    t.text     "text",        limit: 65535
    t.string   "image",       limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "hovers", ["page_id"], name: "index_hovers_on_page_id", using: :btree

  create_table "images", id: false, force: :cascade do |t|
    t.integer  "id",          limit: 4,   null: false
    t.string   "gallery_id",  limit: 255
    t.string   "description", limit: 255
    t.string   "src",         limit: 255, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "position",    limit: 4
  end

  add_index "images", ["gallery_id"], name: "index_images_on_gallery_id", using: :btree

  create_table "joiners", force: :cascade do |t|
    t.integer  "lamp_id",             limit: 4, null: false
    t.integer  "constituent_id",      limit: 4, null: false
    t.integer  "constituent_type_id", limit: 4
    t.integer  "sub_total",           limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "joins", force: :cascade do |t|
    t.integer  "lamp_id",           limit: 4
    t.integer  "constituent_id",    limit: 4
    t.integer  "constituent_count", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "lamps", force: :cascade do |t|
    t.string   "model",       limit: 255
    t.text     "description", limit: 65535
    t.integer  "sell",        limit: 4
    t.integer  "cost",        limit: 4
    t.integer  "stock",       limit: 4
    t.integer  "watt",        limit: 4
    t.integer  "width",       limit: 4
    t.integer  "length",      limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "slug",        limit: 255
    t.integer  "volt",        limit: 4
  end

  create_table "led_configurations", force: :cascade do |t|
    t.integer  "product_id",       limit: 4
    t.integer  "quantity",         limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "led_id",           limit: 4
    t.integer  "series_sum",       limit: 4
    t.integer  "series_led_count", limit: 4
  end

  create_table "led_configurations_leds", id: false, force: :cascade do |t|
    t.integer "led_configuration_id", limit: 4, null: false
    t.integer "led_id",               limit: 4, null: false
  end

  create_table "led_configurations_products", id: false, force: :cascade do |t|
    t.integer "product_id",           limit: 4, null: false
    t.integer "led_configuration_id", limit: 4, null: false
  end

  create_table "led_translations", force: :cascade do |t|
    t.integer  "led_id",      limit: 4,   null: false
    t.string   "locale",      limit: 255, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
  end

  add_index "led_translations", ["led_id"], name: "index_led_translations_on_led_id", using: :btree
  add_index "led_translations", ["locale"], name: "index_led_translations_on_locale", using: :btree

  create_table "leds", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "wl",          limit: 4
    t.string   "wl_suffix",   limit: 255,                          default: "nm"
    t.decimal  "fv",                      precision: 10, scale: 2
    t.decimal  "amp",                     precision: 10, scale: 2
    t.string   "color",       limit: 255
    t.decimal  "price",                   precision: 10, scale: 2
    t.string   "currency",    limit: 255,                          default: "USD"
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.string   "description", limit: 255
  end

  create_table "maps", force: :cascade do |t|
    t.string   "person",     limit: 255
    t.float    "lat",        limit: 24
    t.float    "lng",        limit: 24
    t.string   "address",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "media", force: :cascade do |t|
    t.string   "file_name",  limit: 255
    t.string   "path",       limit: 255
    t.string   "alt",        limit: 255
    t.text     "desc",       limit: 65535
    t.integer  "page_id",    limit: 4
    t.boolean  "feature",    limit: 1
    t.boolean  "display",    limit: 1,     default: true
    t.datetime "updated_at",                              null: false
    t.datetime "created_at",                              null: false
  end

  create_table "messages", force: :cascade do |t|
    t.string   "user_id",    limit: 255
    t.text     "content",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "navigation_translations", force: :cascade do |t|
    t.integer  "navigation_id", limit: 4,   null: false
    t.string   "locale",        limit: 255, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "name",          limit: 255
  end

  add_index "navigation_translations", ["locale"], name: "index_navigation_translations_on_locale", using: :btree
  add_index "navigation_translations", ["navigation_id"], name: "index_navigation_translations_on_navigation_id", using: :btree

  create_table "navigations", force: :cascade do |t|
    t.integer  "site_id",    limit: 4
    t.string   "name",       limit: 255
    t.string   "href",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "order",      limit: 4
    t.boolean  "display",    limit: 1
    t.string   "controller", limit: 255
    t.string   "css_class",  limit: 255
    t.string   "icon",       limit: 255
  end

  create_table "pages", force: :cascade do |t|
    t.integer  "site_id",    limit: 4
    t.string   "title",      limit: 255
    t.string   "permalink",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "slug",       limit: 255
  end

  create_table "product_categories", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "store_id",    limit: 4
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "product_categories", ["store_id"], name: "fk_rails_96ab09ef18", using: :btree

  create_table "product_images", force: :cascade do |t|
    t.integer  "product_id", limit: 4
    t.integer  "order",      limit: 4
    t.string   "alt",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.boolean  "feature",    limit: 1
    t.string   "src",        limit: 255
    t.integer  "lamp_id",    limit: 4
  end

  create_table "product_translations", force: :cascade do |t|
    t.integer  "product_id",  limit: 4,   null: false
    t.string   "locale",      limit: 255, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "description", limit: 255
  end

  add_index "product_translations", ["locale"], name: "index_product_translations_on_locale", using: :btree
  add_index "product_translations", ["product_id"], name: "index_product_translations_on_product_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.integer  "product_category_id",  limit: 4
    t.string   "name",                 limit: 255
    t.text     "description",          limit: 65535
    t.decimal  "price",                              precision: 10
    t.string   "currency",             limit: 255,                  default: "ILS"
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.integer  "led_count",            limit: 4
    t.string   "lighting_area",        limit: 255
    t.integer  "volt",                 limit: 4
    t.integer  "watt",                 limit: 4
    t.integer  "stock",                limit: 4
    t.string   "slug",                 limit: 255
    t.integer  "width",                limit: 4
    t.integer  "length",               limit: 4
    t.integer  "led_configuration_id", limit: 4
    t.string   "user_id",              limit: 255
  end

  add_index "products", ["product_category_id"], name: "fk_rails_ba4de30270", using: :btree
  add_index "products", ["slug"], name: "index_products_on_slug", using: :btree
  add_index "products", ["user_id"], name: "index_products_on_user_id", using: :btree

  create_table "section_translations", force: :cascade do |t|
    t.integer  "section_id", limit: 4,     null: false
    t.string   "locale",     limit: 255,   null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "header",     limit: 255
    t.text     "content",    limit: 65535
  end

  add_index "section_translations", ["locale"], name: "index_section_translations_on_locale", using: :btree
  add_index "section_translations", ["section_id"], name: "index_section_translations_on_section_id", using: :btree

  create_table "sections", force: :cascade do |t|
    t.integer  "page_id",    limit: 4
    t.string   "header",     limit: 255
    t.text     "content",    limit: 65535
    t.string   "image",      limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "sections", ["page_id"], name: "index_sections_on_page_id", using: :btree

  create_table "site_translations", force: :cascade do |t|
    t.integer  "site_id",     limit: 4,     null: false
    t.string   "locale",      limit: 255,   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
  end

  add_index "site_translations", ["locale"], name: "index_site_translations_on_locale", using: :btree
  add_index "site_translations", ["site_id"], name: "index_site_translations_on_site_id", using: :btree

  create_table "sites", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "url",         limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "slug",        limit: 255
  end

  create_table "slides", force: :cascade do |t|
    t.integer  "site_id",    limit: 4
    t.string   "image",      limit: 255
    t.string   "caption",    limit: 255
    t.text     "content",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.boolean  "active",     limit: 1
  end

  add_index "slides", ["site_id"], name: "index_slides_on_site_id", using: :btree

  create_table "snippets", force: :cascade do |t|
    t.string   "language",    limit: 255
    t.text     "snip",        limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.text     "instruction", limit: 65535
  end

  create_table "stores", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "site_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "stores", ["site_id"], name: "fk_rails_06c0d44981", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "uid",              limit: 255
    t.string   "name",             limit: 255
    t.string   "oauth_token",      limit: 255
    t.datetime "oauth_expires_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "provider",         limit: 255
    t.string   "email",            limit: 255
    t.string   "image",            limit: 255
    t.boolean  "admin",            limit: 1
    t.string   "password_digest",  limit: 255
  end

  add_foreign_key "product_categories", "stores"
  add_foreign_key "products", "product_categories"
  add_foreign_key "stores", "sites"
end
