class CreateJoinTable < ActiveRecord::Migration
  def change
    create_join_table :products, :led_configurations do |t|
      # t.index [:product_id, :led_configuration_id]
      # t.index [:led_configuration_id, :product_id]
    end
  end
end
