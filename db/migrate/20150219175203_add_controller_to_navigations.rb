class AddControllerToNavigations < ActiveRecord::Migration
  def change
    add_column :navigations, :controller, :string
  end
end
