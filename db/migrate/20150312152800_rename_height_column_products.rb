class RenameHeightColumnProducts < ActiveRecord::Migration
  def change
	rename_column :products, :height, :length
  end
end
