class AddColumnsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :dimensions, :string
    add_column :products, :led_count, :integer
    add_column :products, :wavelengths, :string
    add_column :products, :lighting_area, :string
    add_column :products, :watt, :integer
  end
end
