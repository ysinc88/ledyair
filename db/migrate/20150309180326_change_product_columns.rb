class ChangeProductColumns < ActiveRecord::Migration
  def change
	add_column :products, :width, :integer
	add_column :products, :height, :integer
	add_column :products, :led_configuration_id, :integer
  end
end
