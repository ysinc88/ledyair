class AddDescriptionColumnToLeds < ActiveRecord::Migration
  def change
	add_column :leds, :description, :string
  end
end
