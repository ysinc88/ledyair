class CreateConstituents < ActiveRecord::Migration
  def change
    create_table :constituents do |t|
      t.string :name
      t.text :description
      t.decimal :cost_ils
      t.decimal :cost_usd
      t.integer :width
      t.integer :length
      t.decimal :volt
      t.decimal :amp
      t.decimal :watt
      t.integer :weight
      t.integer :type_id
      t.integer :cfm

      t.timestamps null: false
    end
    add_index :constituents, :type_id
  end
end
