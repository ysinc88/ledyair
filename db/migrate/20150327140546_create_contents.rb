class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.integer :page_id
      t.text :body

      t.timestamps null: false
    end
    add_index :contents, :page_id
  end
end
