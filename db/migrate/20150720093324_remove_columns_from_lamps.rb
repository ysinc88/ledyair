class RemoveColumnsFromLamps < ActiveRecord::Migration
  def change
	remove_column :lamps, :led_sum1
	remove_column :lamps, :led_sum4
	remove_column :lamps, :led_sum3
	remove_column :lamps, :led_sum2
  end
end
