class AddLampIdToProductImages < ActiveRecord::Migration
  def change
	add_column :product_images, :lamp_id, :integer
  end
end
