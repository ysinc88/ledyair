class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :site_id
      t.string :name

      t.timestamps null: false
    end
    add_index :galleries, :site_id
  end
end
