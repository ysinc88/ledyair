class RenameTypeIdOnConstituents < ActiveRecord::Migration
  def change
	rename_column :constituents, :type_id, :constituent_type_id
  end
end
