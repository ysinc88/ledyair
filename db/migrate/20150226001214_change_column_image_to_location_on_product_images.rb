class ChangeColumnImageToLocationOnProductImages < ActiveRecord::Migration
  def change
	rename_column :product_images, :image, :src
  end
end
