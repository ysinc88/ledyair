class CreateJoinTableLedConfigurationsLeds < ActiveRecord::Migration
  def change
    create_join_table :led_configurations, :leds do |t|
      # t.index [:led_configuration_id, :led_id]
      # t.index [:led_id, :led_configuration_id]
    end
  end
end
