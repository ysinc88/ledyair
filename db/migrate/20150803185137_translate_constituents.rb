class TranslateConstituents < ActiveRecord::Migration
  def self.up
    Constituent.create_translation_table!({
      :name => :string,
      :description => :text
    }, {
      :migrate_data => true
    })
  end

  def self.down
    Constituent.drop_translation_table! :migrate_data => true
  end
end
