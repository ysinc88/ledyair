class AddSeriesInfoColumnsToLedConfigurations < ActiveRecord::Migration
  def change
	add_column :led_configurations, :series_sum, :integer
	add_column :led_configurations, :series_led_count, :integer
  end
end
