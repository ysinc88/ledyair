class AddIconToNavigations < ActiveRecord::Migration
  def change
    add_column :navigations, :icon, :string
  end
end
