class CreateCosts < ActiveRecord::Migration
  def change
    create_table :costs do |t|
      t.integer :category_id
      t.string :item
      t.string :slug
      t.decimal :price

      t.timestamps null: false
    end
  end
end
