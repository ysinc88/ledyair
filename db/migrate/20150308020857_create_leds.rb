class CreateLeds < ActiveRecord::Migration
  def change
    create_table :leds do |t|
      t.integer :product_id
      t.string :name
      t.integer :wl
      t.decimal :fv
      t.decimal :A

      t.timestamps null: false
    end
  end
end
