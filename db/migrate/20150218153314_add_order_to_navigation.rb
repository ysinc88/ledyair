class AddOrderToNavigation < ActiveRecord::Migration
  def change
    add_column :navigations, :order, :integer
    add_column :navigations, :display, :boolean
  end
end
