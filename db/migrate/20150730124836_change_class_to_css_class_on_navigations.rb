class ChangeClassToCssClassOnNavigations < ActiveRecord::Migration
  def change
	rename_column :navigations, :class, :css_class
  end
end
