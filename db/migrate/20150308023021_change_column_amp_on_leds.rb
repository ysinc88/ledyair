class ChangeColumnAmpOnLeds < ActiveRecord::Migration
  def change
	rename_column :leds, :A, :amp 
  end
end
