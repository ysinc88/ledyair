class RenameSumOnJoiners < ActiveRecord::Migration
  def change
	rename_column :joiners, :sum, :sub_total
  end
end
