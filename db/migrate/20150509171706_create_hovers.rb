class CreateHovers < ActiveRecord::Migration
  def change
    create_table :hovers do |t|
      t.string :page_id
      t.string :name
      t.text :description
      t.string :handler
      t.text :text
      t.string :image

      t.timestamps null: false
    end
    add_index :hovers, :page_id
  end
end
