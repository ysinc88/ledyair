class AddPriceToLeds < ActiveRecord::Migration
  def change
	add_column :leds, :price, :decimal
	add_column :leds, :currency, :string, :default => "USD"
  end
end
