class CreateLedConfigurations < ActiveRecord::Migration
  def change
    create_table :led_configurations do |t|
      t.integer :product_id
      t.string :name
      t.integer :wl
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
