class TranslateSections < ActiveRecord::Migration
  def self.up
    Section.create_translation_table!({
      :header => :string,
      :content => :text
    }, {
      :migrate_data => true
    })
  end

  def self.down
    Section.drop_translation_table! :migrate_data => true
  end
end