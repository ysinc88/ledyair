class CreateLamps < ActiveRecord::Migration
  def change
    create_table :lamps do |t|
      t.string :model
      t.text :description
      t.integer :sell
      t.integer :cost
      t.integer :stock
      t.integer :watt
      t.integer :width
      t.integer :length
      t.integer :led_sum1
      t.integer :led_sum2
      t.integer :led_sum3
      t.integer :led_sum4

      t.timestamps null: false
    end
  end
end
