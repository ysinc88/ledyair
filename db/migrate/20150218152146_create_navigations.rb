class CreateNavigations < ActiveRecord::Migration
  def change
    create_table :navigations do |t|
      t.integer :site_id
      t.string :name
      t.string :href

      t.timestamps null: false
    end
  end
end
