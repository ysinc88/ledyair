class CreateMaps < ActiveRecord::Migration
  def change
    create_table :maps do |t|
      t.string :person
      t.float :lat
      t.float :lng
      t.string :address

      t.timestamps null: false
    end
  end
end
