class TranslateNavigations < ActiveRecord::Migration
  def self.up
    Navigation.create_translation_table!({
      :name => :string,
    }, {
      :migrate_data => true
    })
  end

  def self.down
    Navigation.drop_translation_table! :migrate_data => true
  end

end
