class RemoveColumnsFromLedConfigurations < ActiveRecord::Migration
  def change
	remove_column :led_configurations, :name
	remove_column :led_configurations, :wl
  end
end
