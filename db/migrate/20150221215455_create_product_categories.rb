class CreateProductCategories < ActiveRecord::Migration
  def change
    create_table :product_categories do |t|
      t.string :name
      t.integer :store_id
      t.text :description
      
      t.timestamps null: false
    end
  end
end
