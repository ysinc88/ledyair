class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :gallery_id
      t.string :description
      t.string :src

      t.timestamps null: false
    end
    add_index :images, :gallery_id
  end
end
