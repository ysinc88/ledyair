class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.integer :page_id
      t.string :header
      t.text :content
      t.string :image
      t.timestamps null: false
    end
    add_index :sections, :page_id
  end
end
