class AddLedIdToLedConfigurations < ActiveRecord::Migration
  def change
	add_column :led_configurations, :led_id, :integer
  end
end
