class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :product_category_id
      t.string :name
      t.text :description
      t.decimal :price
      t.string :currency
      t.string :image
      
      t.timestamps null: false
    end
  end
end
