class AddClassToNavigations < ActiveRecord::Migration
  def change
    add_column :navigations, :class, :string
  end
end
