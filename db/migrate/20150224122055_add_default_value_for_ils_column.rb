class AddDefaultValueForIlsColumn < ActiveRecord::Migration
  def change
	change_column :products, :currency, :string, :default => "ILS"
  end
end
