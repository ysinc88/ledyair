class AddVoltToLamps < ActiveRecord::Migration
  def change
    add_column :lamps, :volt, :integer
  end
end
