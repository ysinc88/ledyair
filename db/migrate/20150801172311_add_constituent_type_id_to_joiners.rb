class AddConstituentTypeIdToJoiners < ActiveRecord::Migration
  def change
    add_column :joiners, :constituent_type_id, :integer
  end
end
