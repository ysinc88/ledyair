class RemoveColumnProductIdFromLeds < ActiveRecord::Migration
  def change
	remove_column :leds, :product_id
  end
end
