class AddOrderAndFeatureColumnsToProductImages < ActiveRecord::Migration
  def change
	add_column :product_images, :order, :integer
	add_column :product_images, :feature, :boolean
  end
end
