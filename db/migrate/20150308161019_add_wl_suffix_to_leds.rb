class AddWlSuffixToLeds < ActiveRecord::Migration
  def change
	add_column :leds, :wl_suffix, :string, :default => "nm"
  end
end
