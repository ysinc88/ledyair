require 'test_helper'

class ConstituentTypesControllerTest < ActionController::TestCase
  setup do
    @constituent_type = constituent_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:constituent_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create constituent_type" do
    assert_difference('ConstituentType.count') do
      post :create, constituent_type: { description: @constituent_type.description, name: @constituent_type.name }
    end

    assert_redirected_to constituent_type_path(assigns(:constituent_type))
  end

  test "should show constituent_type" do
    get :show, id: @constituent_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @constituent_type
    assert_response :success
  end

  test "should update constituent_type" do
    patch :update, id: @constituent_type, constituent_type: { description: @constituent_type.description, name: @constituent_type.name }
    assert_redirected_to constituent_type_path(assigns(:constituent_type))
  end

  test "should destroy constituent_type" do
    assert_difference('ConstituentType.count', -1) do
      delete :destroy, id: @constituent_type
    end

    assert_redirected_to constituent_types_path
  end
end
