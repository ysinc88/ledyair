require 'test_helper'

class LampsControllerTest < ActionController::TestCase
  setup do
    @lamp = lamps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lamps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lamp" do
    assert_difference('Lamp.count') do
      post :create, lamp: { cost: @lamp.cost, description: @lamp.description, led_sum1: @lamp.led_sum1, led_sum2: @lamp.led_sum2, led_sum3: @lamp.led_sum3, led_sum4: @lamp.led_sum4, length: @lamp.length, model: @lamp.model, sell: @lamp.sell, stock: @lamp.stock, watt: @lamp.watt, width: @lamp.width }
    end

    assert_redirected_to lamp_path(assigns(:lamp))
  end

  test "should show lamp" do
    get :show, id: @lamp
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lamp
    assert_response :success
  end

  test "should update lamp" do
    patch :update, id: @lamp, lamp: { cost: @lamp.cost, description: @lamp.description, led_sum1: @lamp.led_sum1, led_sum2: @lamp.led_sum2, led_sum3: @lamp.led_sum3, led_sum4: @lamp.led_sum4, length: @lamp.length, model: @lamp.model, sell: @lamp.sell, stock: @lamp.stock, watt: @lamp.watt, width: @lamp.width }
    assert_redirected_to lamp_path(assigns(:lamp))
  end

  test "should destroy lamp" do
    assert_difference('Lamp.count', -1) do
      delete :destroy, id: @lamp
    end

    assert_redirected_to lamps_path
  end
end
