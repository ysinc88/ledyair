require 'test_helper'

class JoinersControllerTest < ActionController::TestCase
  setup do
    @joiner = joiners(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:joiners)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create joiner" do
    assert_difference('Joiner.count') do
      post :create, joiner: { constituent_id: @joiner.constituent_id, lamp_id: @joiner.lamp_id, sum: @joiner.sum }
    end

    assert_redirected_to joiner_path(assigns(:joiner))
  end

  test "should show joiner" do
    get :show, id: @joiner
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @joiner
    assert_response :success
  end

  test "should update joiner" do
    patch :update, id: @joiner, joiner: { constituent_id: @joiner.constituent_id, lamp_id: @joiner.lamp_id, sum: @joiner.sum }
    assert_redirected_to joiner_path(assigns(:joiner))
  end

  test "should destroy joiner" do
    assert_difference('Joiner.count', -1) do
      delete :destroy, id: @joiner
    end

    assert_redirected_to joiners_path
  end
end
