This is a RoR project for my LED Grow light business.
It was built from scratch and includes a simple self developed CMS for rapid new website(s) development.
I used Figaro for keeping the secrets and storing ENV variables.
After cloning, run figaro install and edit config/application.yml

